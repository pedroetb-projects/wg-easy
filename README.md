# wg-easy

The easiest way to run WireGuard VPN + Web-based Admin UI

## Client usage

### Linux

Install with `apt install wireguard`, download client configuration file and save it as `/etc/wireguard/wg0.conf`. Then, you can run `wg-quick up wg0` to establish connection or `wg-quick down wg0` to close established connection.

Note that file name is used as connection name, `wg0` in this example. You can choose another name renaming config file.

In order to ease usage, you can import configuration to your network manager.

```sh
sudo nmcli connection import type wireguard file "/etc/wireguard/wg0.conf"
```

Now, it shows up permanently in your saved network connections at notification area. You can toggle connection easily without using `sudo`.

By default, this will enable VPN connection at boot time. If you want to disable this, run:

```sh
nmcli con mod wg0 connection.autoconnect no
```

### Android and iOS

Install app from [Play Store](https://play.google.com/store/apps/details?id=com.wireguard.android) or [App Store](https://apps.apple.com/es/app/wireguard/id1441195209) and scan QR code.

## Fix network addresses collision

If you can't connect to local devices at vpn server local network, the problem may be a collision with client local network devices. After reading <https://blog.vforvruno.cyou/wireguard-local-network-unreachable>, I tried allowing specific address ranges (although may appear redundant, it's not when you have address collision).

At vpn client local network, I'm using `10.0.0.0/10` (addresses from *10.0.0.1* to *10.63.255.254*), and at server local network, I'm using `10.0.0.0/8` (addresses from *10.0.0.1* to *10.255.255.254*). So when trying to connect to `10.0.0.1` after setting up wireguard connection, I got response from router at client local network, instead of router at server local network.

After setting client configuration with `AllowedIPs = 0.0.0.0/0, ::/0, 10.0.0.0/10`, connecting to `10.0.0.1` began to get response from router at server local network! Also works with masks greater than */10* (and lower than */32*, obviously).

This wouldn't be necessary if you are using different IP ranges at both ends, but if you do you always can allow this traffic explicitly.
